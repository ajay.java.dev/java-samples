<h1>Java 12 features</h1>

<ol>
    <li><h5>Switch expressions (JEP 325) (SwitchExpressions.java    ) -  will 
    improve coding by extending
     the switch statement, enabling its use as either a statement or an expression.</h5></li>     
    <li><h5>Default CDS archives : https://openjdk.java.net/jeps/350</h5></li>
    <li><h5>Shenandoah: A low-pause-time garbage collector (JEP 189)</h5></li>
    <li><h5>Microbenchmark suite</h5></li>
    <li><h5>JVM constants API</h5></li>
    <li><h5>One AArch64 port, not two</h5></li>
    <li><h5>Abortable mixed collections for G1</h5></li>
    <li><h5>Promptly return unused committed memory from G1</h5></li>
</ol>

