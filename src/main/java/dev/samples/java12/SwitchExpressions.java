package dev.samples.java12;

// Sample from :
public class SwitchExpressions {

	private static final String PEAR = "PEAR";

	private static final String APPLE = "APPLE";
	private static final String GRAPE = "GRAPE";
	private static final String MANGO = "MANGO";

	private static final String ORANGE = "ORANGE";
	private static final String PAPAYA = "PAPAYA";

	public int oldSwitch (String fruit) {
		int returnVal = 0;
		switch (fruit) {
			case PEAR:
				returnVal = PEAR.length();
				break;
			case APPLE:
			case GRAPE:
			case MANGO:
				returnVal = APPLE.length();
				break;
			case ORANGE:
			case PAPAYA:
				returnVal = ORANGE.length();
				break;
			default:
				returnVal = 0;
		}
		return returnVal;
	}

	public int newSwitch (String fruit)  {
		return switch (fruit){
			case PEAR -> PEAR.length();
			case APPLE, GRAPE, MANGO -> GRAPE.length();
			case ORANGE, PAPAYA -> ORANGE.length();
			default -> 0;
		};
	}

	public static String mask (String s) {
		return "Inside ... " + s;
	}

	public static void main(String[] args){

		System.out.println("SwitchExpressions.main".transform(SwitchExpressions::mask));

		SwitchExpressions se = new SwitchExpressions();
		System.out.println("Val 1 : ".indent(20) + se.oldSwitch(APPLE));
		System.out.println("Val 2 : ".indent(20) + se.newSwitch(ORANGE));
		System.out.println("".indent(20) + se.newSwitch(PEAR));
	}
}
