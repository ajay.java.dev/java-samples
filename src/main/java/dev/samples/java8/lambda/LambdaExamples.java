package dev.samples.java8.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class LambdaExamples {

  public static void streamSample1() {

    List numbers = Arrays.asList(10, 12, 13);

    // 1) Write the predicate first
    Predicate<Integer> predicate =
        new Predicate<Integer>() {
          @Override
          public boolean test(Integer number) {
            return (number % 2) != 0;
          }
        };

    // 2) Create the stream and call functions.
    Stream<Integer> stream = numbers.stream().filter(predicate);
      // Note : reduce needs to be part of filter chaining else stream is closed
    int val = stream.filter(predicate).reduce(0, Integer::sum);

    System.out.println("LambdaExamples.streamSample1 ... " + val);
  }

  public static void main(String[] args) {
	  streamSample1();
  }
}
