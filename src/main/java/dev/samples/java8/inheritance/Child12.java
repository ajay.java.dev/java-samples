package dev.samples.java8.inheritance;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

// Static and default methods can be created inside Interfaces.
// -- static - implementation remains within the interface
// -- default -- implementation must be overriden in the Child class.
public class Child12 implements Interface1, Interface2 {

  @Override
  public void method1(String str) {
    System.out.print("Child12.method1 ... ");
    System.out.println("str = [" + str + "]");
  }

  @Override
  public void method2() {
    System.out.println("Child12.method2");
  }

  @Override
  public void log(String str) {
    System.out.println("str = [" + str + "]");
    Interface1.print("abc");
    // or
    Interface1 interface1 = (s) -> System.out.println("Child12.log");
    interface1.method1("Testing new method...");
  }

  @Override
  public String showListEntries(List<String> list) {
    list.forEach(
        new Consumer<String>() {
          @Override
          public void accept(String val) {
            System.out.println("Java8ForEachExample.accept --> value : " + val);
          }
        });

    StringBuilder sb = new StringBuilder();

    // statement lambda replaced with lambda expression
    list.forEach(item -> sb.append(item).append(","));

    return sb.toString();
  }

  public static void main(String[] args) {
    Child12 child12 = new Child12();
    child12.log("Test 123");
    child12.method1("Test 123");
    child12.method2();

    List<String> newList = new ArrayList<>();
    newList.add("Test1");
    newList.add("Test2");
    newList.add("Test3");
    newList.add("Test4");
    newList.add("Test5");

    System.out.println("Child12.main.showListEntries --> " + child12.showListEntries(newList));
  }
}
