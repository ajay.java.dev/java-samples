package dev.samples.java8.inheritance;

import java.util.List;

// We know that Java doesn’t provide multiple inheritance in Classes
// because it leads to Diamond Problem
@FunctionalInterface
public interface Interface2 {

  void method2();

  default void log(String str) {
    System.out.println("Interface2.log -->" + str);
  }

  default String showListEntries(List<String> list) {
    return null;
  }
}
