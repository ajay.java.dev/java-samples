package dev.samples.java8.inheritance;

import java.util.List;

// We know that Java doesn’t provide multiple inheritance in Classes
// because it leads to Diamond Problem
@FunctionalInterface
public interface Interface1 {

  void method1(String str);

  default void log(String str) {
    System.out.println("Interface1.log -->" + str);
  }

  // static belongs to the class or now to this interface.
  static void print(String str) {
    System.out.println("Printing 1 --> " + str);
  }

  default String showListEntries(List<String> list) {
    return null;
  }
}
