package dev.samples.java8.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ParallelStream {
  public static void main(String[] args) {
	  List<Integer> list = new ArrayList<>();

	  for (int i = 0; i < 1000; i++) {
		  list.add(i);
	  }

	  Stream<Integer> sequentialStream = list.stream();
	  Stream<Integer> highNumsSeq = sequentialStream.filter(p -> p > 90);
	  //highNums.forEach(p -> System.out.println("High Nums sequential="+p));

	  Stream<Integer> parallelStream = list.parallelStream();
	  Stream<Integer> highNums = parallelStream.filter(p -> p > 90);

	  //used for working with huge collections...
	  highNums.forEach(p -> System.out.println("High Nums parallel="+p));



  }
}
