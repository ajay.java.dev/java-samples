package dev.samples.java8.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

// helps in having our business logic at a separate location that we can reuse
public class ForEachExample {

  public static void main(String[] args) {
    List<Integer> list = new ArrayList<>();

    for (int i = 0; i < 10; i++) {
      list.add(i);
    }

    Iterator<Integer> it = list.iterator();

    while (it.hasNext()) {
      System.out.println("Java8ForEachExample.main --> iterator value" + it.next());
    }

    // anonymous class approach
    list.forEach(
        new Consumer<Integer>() {
          @Override
          public void accept(Integer integer) {
            System.out.println("Java8ForEachExample.accept --> value : " + integer);
          }
        });

    MyConsumer consumer = new MyConsumer();
    list.forEach(consumer);
  }
}

// business logic at a separate location that we can reuse. This helps in
// higher seperation of concern and cleaner code.
// This is nice but what about thread safety ?
class MyConsumer implements Consumer<Integer> {
  @Override
  public void accept(Integer integer) {
    System.out.println("MyConsumer.accept ...");
    System.out.println("integer = [" + integer + "]");
  }
}

/*
interface MyConsumerDefault {
  //here forEach is an Extension method...
  default void forEach(Consumer<? super T> action) {
    Objects.requireNonNull(action);
    for (T t : this) {
      action.accept(t);
    }
  }
}
*/
