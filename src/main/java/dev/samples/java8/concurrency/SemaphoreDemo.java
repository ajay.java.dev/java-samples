package dev.samples.java8.concurrency;

import java.util.concurrent.*;

/*
  Here the expectation was that the semaphore would be able
 */

class Counter {
  static int count = 0;
}

// Incrementing Thread...
class IncThread implements Runnable {

  private String name;
  private Semaphore semaphore;

  public IncThread(Semaphore semaphore, String name) {
    this.name = name;
    this.semaphore = semaphore;
  }

  @Override
  public void run() {
    try {
      System.out.println("IncThread.run");
      System.out.println(name + " is waiting to get the permit");

      semaphore.acquire();

      System.out.println(
          name + " acquires permit, available permits : " + semaphore.availablePermits());
      System.out.println(name + " acquires thread");
      for (int i = 0; i < 5; i++) {
        Counter.count++;

        System.out.println("Inc Count : " + Counter.count);

        //Thread.sleep(1000L);
      }

      semaphore.release();

      System.out.println("IncThread.run --> semaphore released()");

      Thread.sleep(1000L);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}

// Decrementing thread...
class DecThread implements Runnable {

  private String name;
  private Semaphore semaphore;

  public DecThread(Semaphore semaphore, String name) {
    this.name = name;
    this.semaphore = semaphore;
  }

  @Override
  public void run() {
    try {
      System.out.println("DecThread.run");
      System.out.println(name + " is waiting to get the permit");

      semaphore.acquire();

      System.out.println(
          name + " acquires permit, available permits : " + semaphore.availablePermits());

      for (int i = 0; i < 5; i++) {

        Counter.count--;

        System.out.println("Dec Count : " + Counter.count);

        //Thread.sleep(1000L);
      }

      semaphore.release();
      System.out.println("IncThread.run --> semaphore released()");

      Thread.sleep(1000L);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}

public class SemaphoreDemo {

  public static void main(String[] args) {
    Semaphore semaphore = new Semaphore(1); // only 1 permit

    ThreadPoolExecutor threadPoolExecutor =
            (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

    for (int i = 0; i < 100; i++) {
      //loop here and pass the inputs required. Use thread pool executor is needed
      //new Thread(new IncThread(semaphore, "A")).start();
      //new Thread(new DecThread(semaphore, "B")).start();

      threadPoolExecutor.execute(new IncThread(semaphore, "A"));
      threadPoolExecutor.execute(new DecThread(semaphore, "B"));
    }

    threadPoolExecutor.shutdown();

    /*

    try {
      while (threadPoolExecutor.awaitTermination(60L, TimeUnit.SECONDS)) {
        System.out.println("Awaiting threadPoolExecutor shutdown ...");
      }
      System.out.println("threadPoolExecutor shutdown complete");
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    */

  }
}
