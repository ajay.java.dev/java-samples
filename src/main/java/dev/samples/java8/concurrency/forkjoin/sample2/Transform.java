package dev.samples.java8.concurrency.forkjoin.sample2;


import java.util.concurrent.RecursiveAction;

public class Transform extends RecursiveAction {

	int threshold;
	double [] data;
	int start, end;

	public Transform (double [] vals, int start, int end, int t){
		this.data = vals;
		this.start = start;
		this.end = end;
		this.threshold = t;
	}

	@Override
	protected void compute() {
		if ((end - start) < threshold) {
			for (int i = start; i < end; i++) {
				if ((data[i] % 2) == 0) {
					data[i] = Math.sqrt(data[i]);
				} else {
					data[i] = Math.cbrt(data[i]);
				}
			}
		} else {
			int middle = (start + end) /2 ;
			invokeAll(new Transform(data, start, middle, threshold),
					new Transform(data, middle, end, threshold));
		}
	}
}
