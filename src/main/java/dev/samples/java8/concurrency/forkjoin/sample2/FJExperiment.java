package dev.samples.java8.concurrency.forkjoin.sample2;

import java.util.concurrent.ForkJoinPool;

public class FJExperiment {

  public static void main(String[] args) {
    int pLevel;
    int threshold;

    if (args.length != 2) {
      System.out.println("Usage: FJExperiment parallelism threshold");
      return;
    }

    pLevel = Integer.parseInt(args[0]);
    threshold = Integer.parseInt(args[1]);

    long beginT, endT;

    ForkJoinPool forkJoinPool = new ForkJoinPool(pLevel);

    double[] nums = new double[1000000];

    for (int i = 0; i < nums.length; i++) {
      nums[i] = (double) i;
    }

    Transform task = new Transform(nums, 0, nums.length, threshold);

    beginT = System.nanoTime();

    forkJoinPool.invoke(task);

    endT = System.nanoTime();

    System.out.println("Level of parallelism: " + pLevel);
    System.out.println("Sequential threshold: " + threshold);
    System.out.println("Elapsed time: " + (endT - beginT) + " ms");
  }
}
