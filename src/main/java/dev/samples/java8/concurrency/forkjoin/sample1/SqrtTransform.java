package dev.samples.java8.concurrency.forkjoin.sample1;

import java.util.concurrent.RecursiveAction;

public class SqrtTransform extends RecursiveAction {

	final int threshHold = 1000;
	double [] data;

	int start,end; //determines which part of data to process

	public SqrtTransform(double [] data, int start, int end) {
		this.data = data;
		this.start = start;
		this.end = end;
	}

	//method where the parallel execution occurs
	@Override
	protected void compute() {
		if ((end - start) < threshHold) {
			for (int i = start; i < end; i++) {
				data[i] = Math.sqrt(data[i]);
			}
		} else {

			//otherwise continue to break the data into smaller pieces.
			int middle = (start + end) / 2;
			//invoke fxn using subdivided data. Divide and Rule...
			invokeAll(new SqrtTransform(data, start, middle),
					new SqrtTransform(data,middle,end));
		}
	}
}
