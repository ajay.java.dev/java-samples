package dev.samples.java8.concurrency;


public class RunnableIn8 {

  public static void main(String[] args) {
    // 1) old approach
    Runnable runnable =
        new Runnable() {
          @Override
          public void run() {
            System.out.println("My Runnable old approach");
          }
        };

    // 2) new approach
    Runnable runnable1 =
        () -> {
          System.out.println("My Runnable new approach");
        };

	  Runnable runnable2 = () -> System.out.println("My Runnable 2nd new " +
			  "approach");

    Thread thread = new Thread(runnable);
    Thread thread1 = new Thread(runnable1);
    Thread thread2 = new Thread(runnable1);
    thread.start();
    thread1.start();
    thread2.start();

  }
}
