package dev.samples.java5;

public class PrintAToZ {

  public static void main(String[] args) {
    //
    System.out.println("\nPrintAToZ.main --> CAP LETTERS... \n");
    for (char c = 'A'; c <= 'Z'; c++) {
      System.out.print(c + " , ");
    }

    System.out.println("\nPrintAToZ.main --> SMALL LETTERS... \n");
    for (char c = 'a'; c <= 'z'; c++) {
      System.out.print(c + " , ");
    }

    System.out.println("\nPrintAToZ.all --> ALL LETTERS... \n");
    for (char c = '0'; c <= 'z'; c++) {
      System.out.print(c + " , ");
    }
  }
}
